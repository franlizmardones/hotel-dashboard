import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CountriesChart from './CountriesChart';
import RoomsChart from './RoomsChart';
import NightsChart from './NightsChart';
import ReservationChart from './ReservationChart';
import Grid from '@mui/material/Grid';
  
export default () => (
    <>  

        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Card>
                    <CardHeader title="Panel de Control - Indicadores Hoteleria" />
                </Card>
            </Grid>
            <Grid item xs={6}>
                <Card>
                    <CardHeader title="Top 5 Most Popular Countries" />
                    <CountriesChart />
                </Card>
            </Grid>
            <Grid item xs={6}>
                <Card>
                    <CardHeader title="Top 5 Most Popular Rooms" />
                    <RoomsChart />
                </Card>
            </Grid>
            <Grid item xs={6}>
                <Card>
                    <CardHeader title="Nights Spent" />
                    <NightsChart />
                </Card>
            </Grid>
            <Grid item xs={6}>
                <Card>
                    <CardHeader title="Reservation Status" />
                    <ReservationChart />
                </Card>
            </Grid>
        </Grid>

    </>
    )