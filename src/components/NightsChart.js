import * as React from 'react';
import { GET_NIGHTS_SPENT } from '../queries/nightsSpent';
import { useQuery } from '@apollo/client';
import { LegendComponent } from 'echarts/components';

// import the core library.
import ReactEChartsCore from 'echarts-for-react/lib/core';
// Import the echarts core module, which provides the necessary interfaces for using echarts.
import * as echarts from 'echarts/core';
// Import charts, all with Chart suffix
import {
    PieChart
} from 'echarts/charts';
// import components, all suffixed with Component
import {
  GridComponent,
  TooltipComponent,
  TitleComponent
} from 'echarts/components';
// Import renderer, note that introducing the CanvasRenderer or SVGRenderer is a required step
import {
  CanvasRenderer,
} from 'echarts/renderers';

// Register the required components
echarts.use(
  [TitleComponent, TooltipComponent, GridComponent, PieChart, CanvasRenderer, LegendComponent]
);

// The usage of ReactEChartsCore are same with above.

const NightsChart = () => {

    const { loading, error, data } = useQuery(GET_NIGHTS_SPENT)

    if (loading) return 'Loading...'
    if (error) return 'Error...'

    const option = {
        tooltip: {
            trigger: 'item'
        },
        legend: {
            orient: 'vertical',
            left: '40'
        },
        series: [{
            name: 'Nights Spent',
            type: 'pie',
            radius: '70%',
            data: data.nights_spent,
            emphasis: {
                itemStyle: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }]
    }

    return (
        <ReactEChartsCore
            echarts={echarts}
            option={option}
        />
    )

}



  export default NightsChart

