import * as React from 'react';
import { GET_RESERVATION_STATUS } from '../queries/reservationStatus';
import { useQuery } from '@apollo/client';

// import the core library.
import ReactEChartsCore from 'echarts-for-react/lib/core';
// Import the echarts core module, which provides the necessary interfaces for using echarts.
import * as echarts from 'echarts/core';
// Import charts, all with Chart suffix
import {
  BarChart
} from 'echarts/charts';
// import components, all suffixed with Component
import {
  GridComponent,
  TooltipComponent,
  TitleComponent
} from 'echarts/components';
// Import renderer, note that introducing the CanvasRenderer or SVGRenderer is a required step
import {
  CanvasRenderer,
} from 'echarts/renderers';

// Register the required components
echarts.use(
  [TitleComponent, TooltipComponent, GridComponent, BarChart, CanvasRenderer]
);

// The usage of ReactEChartsCore are same with above.

const CountriesChart = () => {

    const { loading, error, data } = useQuery(GET_RESERVATION_STATUS)

    if (loading) return 'Loading...'
    if (error) return 'Error...'
    
    const countryArray = data.reservation_status.map(x => x.resv_status)
    const countArray = data.reservation_status.map(x => x.count)

    console.log(countryArray)
    console.log(countArray)

    const option = {
        color: ["#3398DB"],
        tooltip: {
          trigger: "axis",
          axisPointer: {
            type: "shadow"
          }
        },
        grid: {},
        xAxis: [{
          type: "category",
          data: countryArray,
          axisTick: {
            alignWithLabel: true
          }
        }],
        yAxis: [{
          type: "value"
        }],
        series: [{
          type: "bar",
          barWidth: "60%",
          data: countArray
        }]
      }

    return (
        <ReactEChartsCore
            echarts={echarts}
            option={option}
        />
    )

}



  export default CountriesChart

