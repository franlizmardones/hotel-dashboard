import React, { Suspense }from 'react'
import { List, Datagrid, TextField } from 'react-admin'
import { Card as MuiCard, CardContent, withStyles } from '@material-ui/core'
import HotelFilters from '../filters/HotelFilters'

const Card = withStyles(theme => ({
    root: {
        [theme.breakpoints.up('sm')]: {
            order: -1, // display on the left rather than on the right of the list
            width: '15em',
            marginRight: '1em',
        },
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
    },
}))(MuiCard);

const FilterSidebar = () => {
    return (<Card>
        <CardContent>
            <Suspense fallback={<div>Loading...</div>}>
                <HotelFilters />
            </Suspense>
        </CardContent>
    </Card>)
}


const HotelList = (props) => {
    return <List {...props} aside={<FilterSidebar />} sort={{ field: 'resv_name_id', order: 'ASC' }}>
        <Datagrid>
            <TextField source="id" sortBy="resv_name_id" />
            <TextField source="resort" sortBy="resort" />
            <TextField source="confirmation_num" sortBy="confirmation_no" />
            <TextField source="status" sortBy="RESV_STATUS " />
            <TextField source="arrival" sortBy="arrival" />
            <TextField source="departure" sortBy="departure" />
            <TextField source="nights" sortBy="nights" />
            <TextField source="adults" sortBy="adults" />
            <TextField source="children" sortBy="children" />
            <TextField source="room" sortBy="room_desc" />
            <TextField source="country" sortBy="country_desc " />
        </Datagrid>
    </List>
}

export default HotelList