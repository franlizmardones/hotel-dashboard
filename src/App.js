import React, { useState, useEffect } from 'react'
import { Admin, Resource } from 'react-admin'
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client'
import buildHasuraProvider from 'ra-data-hasura'
import HotelList from './components/HotelList'
import customBuildFields from './custom-build-fields';
import Dashboard from './components/Dashboard';

const myClientWithAuth = new ApolloClient({
  uri: 'https://hotel-dashboard-test.hasura.app/v1/graphql',
  cache: new InMemoryCache(),
  headers: {
    'x-hasura-admin-secret': 'lmqJJUXQ8R5CdqzZcRYRUp6h4kYqC9GA2nhybyQHpJlhlbdUyH5UXbEFEihyqqGX'
  },
})

function App() {
  const [dataProvider, setDataProvider] = useState(null)
  
  useEffect(() => {
    const buildDataProvider = async () => {
      const dataProvider = await buildHasuraProvider({
        client: myClientWithAuth
      },
      { buildFields: customBuildFields })
      setDataProvider(() => dataProvider)
    }
    buildDataProvider();
  }, [])

  if (!dataProvider) return <p>Loading...</p>;
  
  return (
    <ApolloProvider client={ myClientWithAuth }>
      <Admin
        title="Dashboard"
        dashboard={Dashboard}
        dataProvider={dataProvider}
        >
          <Resource
            name="datosreserva"
            list={HotelList}
          />
      </Admin>
    </ApolloProvider>

  )
}

export default App;
