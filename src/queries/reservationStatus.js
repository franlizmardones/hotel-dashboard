import gql from 'graphql-tag';

export const GET_RESERVATION_STATUS = gql`
query ReservationStatus {
    reservation_status {
      resv_status
      count
    }
  }`