import gql from 'graphql-tag';

export const GET_DATOS_RESERVA = gql`
{
    id: resv_name_id
    resort: resort
    confirmation_num: confirmation_no
    status: resv_status
    arrival: arrival
    departure: departure
    nights: nights
    adults: adults
    children: children
    room: room_desc
    country: country_desc
}
`;