import gql from 'graphql-tag';

export const GET_NIGHTS_SPENT = gql`
query NightsSpent {
  nights_spent {
    name: noches
    value: count
  }
}`