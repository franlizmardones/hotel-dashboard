import gql from 'graphql-tag';

export const GET_TOP_5_COUNTRIES = gql`
query Top5Countries {
    top_5_country_reservations {
      country_desc
      count
    }
}`