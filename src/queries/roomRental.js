import gql from 'graphql-tag';

export const GET_TOP_5_ROOMS = gql`
query Top5Rooms {
    top_5_rooms {
      room_desc
      count
    }
  }`