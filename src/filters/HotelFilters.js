import * as React from 'react'
import { useHistory } from 'react-router-dom'
import { useLocation } from 'react-router'
import { useQuery } from '@apollo/client'
import gql from 'graphql-tag'
import qs from 'query-string';

import { default as MuiList } from '@mui/material/List'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Collapse from '@mui/material/Collapse'
import ExpandLess from '@mui/icons-material/ExpandLess'
import ExpandMore from '@mui/icons-material/ExpandMore'
import PublicIcon from '@mui/icons-material/Public'
import { Button } from '@mui/material';

const Filters = () => {

    const [openReserv, setOpenReserv] = React.useState(true);
    const [openCountries, setOpenCountries] = React.useState(true);

    const history = useHistory();
    const location = useLocation();
    const queryParams = qs.parse(location.search);
    let filterParams = {}

    if (queryParams.filter) {
        filterParams = JSON.parse(queryParams.filter);
    }

    const handleCountryOpenClick = () => {
        setOpenCountries(!openCountries);
    };

    const handleReservOpenClick = () => {
        setOpenReserv(!openReserv);
    };

    const handleCountryClick = (event, country) => {
        filterParams.country_desc  = country;
        const newQueries = { ...queryParams, filter: JSON.stringify(filterParams) };
        history.push({ search: qs.stringify(newQueries) });
    };

    const handleReservClick = (event, reserv) => {
        filterParams.resv_status  = reserv;
        delete filterParams['country_desc']; 
        const newQueries = { ...queryParams, filter: JSON.stringify(filterParams) };
        history.push({ search: qs.stringify(newQueries) });
    };

    const GET_RESERV = gql`
        query GetReserv {
            datosreserva(distinct_on: resv_status) {
                resv_status
            }
    }`

    const { loading: loadingStatus, error: errorStatus, data: dataStatus } = useQuery(GET_RESERV)

    let filterVariables = {}
    
    let GET_COUNTRIES = null

    if (filterParams.resv_status) {
        GET_COUNTRIES = gql`
        query GetCountries ($reserv: String) {
            datosreserva(distinct_on: country_desc, where: {resv_status: {_eq: $reserv}}) {
                country_desc
            }
        }`
        filterVariables =  {
            variables: {reserv: filterParams.resv_status}
        }
        console.log('IF')
    }else{
        GET_COUNTRIES = gql`
        query GetCountries {
            datosreserva(distinct_on: country_desc) {
                country_desc
            }
        }`
        console.log('ELSE')
    }

    const { loading: loadingCountries, error: errorCountries, data: dataCountries } = useQuery(GET_COUNTRIES, filterVariables)

    if (loadingStatus) return 'Loading...'
	if (errorStatus) return `Error: ${errorCountries.message}`
    if (loadingCountries) return 'Loading...'
	if (errorCountries) return `Error: ${errorCountries.message}`

    const clearFilters = () => {
        console.log(filterParams)
        delete filterParams['country_desc'];
        delete filterParams['resv_status'];
        const newQueries = { ...queryParams, filter: JSON.stringify(filterParams) };
        history.push({ search: qs.stringify(newQueries) });
        
    }

	return (
		<>
            <ListItemButton onClick={handleReservOpenClick}>
                <ListItemIcon>
                    <PublicIcon />
                </ListItemIcon>
                <ListItemText primary="Reservation" />
                {openReserv ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={openReserv} timeout="auto" unmountOnExit>
                <MuiList component="div" disablePadding>
                    {dataStatus.datosreserva.map(item => (
                        <ListItemButton sx={{ pl: 4 }} 
                            key={item.resv_status }
                            selected={filterParams.resv_status === item.resv_status }
                            onClick={(event) => handleReservClick(event, item.resv_status )}>
                            <ListItemText primary={item.resv_status } />
                        </ListItemButton>
                    ))} 
                </MuiList>
            </Collapse>

            <ListItemButton onClick={handleCountryOpenClick}>
                <ListItemIcon>
                    <PublicIcon />
                </ListItemIcon>
                <ListItemText primary="Countries" />
                {openCountries ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={openCountries} timeout="auto" unmountOnExit>
                <MuiList component="div" disablePadding>
                    {dataCountries.datosreserva.map(item => (
                        <ListItemButton sx={{ pl: 4 }} 
                            key={item.country_desc}
                            selected={filterParams.country_desc === item.country_desc}
                            onClick={(event) => handleCountryClick(event, item.country_desc)}>
                            <ListItemText primary={item.country_desc} />
                        </ListItemButton>
                    ))}
                </MuiList>
            </Collapse>
            <Button variant="contained" onClick={clearFilters}>Limpiar Filtros</Button>
			
            
		</>
	)
}



export default Filters